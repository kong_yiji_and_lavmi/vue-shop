# Vue_Shop(VShop)移动端 （开发中...）

本项目使用[Nuxt.js 2.x](https://www.nuxtjs.cn/)+[Vant UI](https://youzan.github.io/vant/#/zh-CN)框架进行开发，一个仿照`淘宝App端`部分功能的商城项目，同时还支持[SEO](https://baike.baidu.com/item/%E6%90%9C%E7%B4%A2%E5%BC%95%E6%93%8E%E4%BC%98%E5%8C%96/3132)。本项目仅供学习使用。

## 预览地址

[Vue_Shop](http://shop.z3web.cn/)

## 使用

1. 使用 git 拉取本项目

```bash
D:> git clone https://gitee.com/kong_yiji_and_lavmi/vue-shop.git
```

2. 安装依赖包

```bash
D:\vue_shop> npm i
# or
D:\vue_shop> cnpm i
```

3. 启动

```bash
D:\vue_shop> npm run dev
```

浏览器输入网址 http://localhost:3333 即可

## 若无本地接口请使用线上接口

修改当前项目的`.env-cmdrc.json`JSON 文件

```json
{
  "development": {
    // 开发模式
    "BASEURL": "http://127.0.0.1:8081", // ajax 请求地址
    "PORT": 3333, // 本地启动端口
    "HOST": "0.0.0.0", // 本地启动地址
    "IMG": "http://127.0.0.1:8888/" // img图片地址
  },
  "production": {
    // 生产模式
    "BASEURL": "//z3web.cn",
    "PORT": 3333,
    "HOST": "0.0.0.0",
    "IMG": "http://127.0.0.1:8888/"
  }
}
```

BASEURL 使用 `https://z3web.cn`。IMG 使用 `https://img.z3web.cn/`