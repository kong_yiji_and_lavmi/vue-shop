import { resolve } from "path";
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Vue_Shop",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    script: [{ src: "/js/fontsize.js" }, { src: "/js/tj.js" }]
  },
  srcDir: "app/",
  css: ["vant/lib/index.css", "~/assets/css/global.css"],
  plugins: [
    "~/plugins/handle-error",
    "~/helper/index.js",
    "~/plugins/vant",
    "~/plugins/axios",
    "~/api/index.js",
    { src: "~/plugins/store", mode: "client" },
    { src: "~/plugins/swiper.js", mode: "client" }
  ],
  modules: ["@nuxtjs/axios"],
  components: true,
  serverMiddleware: [
    {
      path: "/local",
      handler: resolve(process.cwd(), "./server/index.js")
    }
  ],
  build: {
    // px 自动转换 rem  "postcss-pxtorem": "^5.1.1",
    postcss: {
      plugins: {
        "postcss-pxtorem": {
          rootValue: 50,
          propList: ["*"],
          minPixelValue: 2
        }
      }
    },

    babel: {
      plugins: [
        [
          "import",
          {
            libraryName: "vant",
            style: name => {
              const baseName = [
                "vant/lib/form",
                "vant/lib/lazyload",
                "vant/lib/area"
              ];
              if (baseName.includes(name)) {
                return "vant/lib/style/base.css";
              }
              return `${name}/index.css`;
            }
          },
          "vant"
        ]
      ]
    }
  },
  transition: {
    name: "fade",
    mode: "out-in"
  },
  server: {
    port: process.env.PORT,
    host: process.env.HOST
  },
  axios: {
    proxy: true
  },
  proxy: {
    "/api": {
      target: process.env.BASEURL,
      pathRewrite: { "^/api/": "/api/vshop/" }
    }
  },
  env: {
    IMG: process.env.IMG
  },
  loading: "~/components/layout/load.vue"
};
