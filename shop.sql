/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50624
 Source Host           : localhost:3306
 Source Schema         : shop

 Target Server Type    : MySQL
 Target Server Version : 50624
 File Encoding         : 65001

 Date: 24/11/2021 18:23:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `u_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `pswd` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录密码',
  `creat_time` datetime NOT NULL COMMENT '创建时间',
  `login_time` datetime NULL DEFAULT NULL COMMENT '登录时间',
  PRIMARY KEY (`u_id`) USING BTREE,
  UNIQUE INDEX `u_email`(`email`) USING BTREE COMMENT '唯一邮箱',
  UNIQUE INDEX `u_phone`(`phone`) USING BTREE COMMENT '唯一电话'
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `address_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '地址id',
  `u_id` int(4) NOT NULL COMMENT '用户id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人姓名',
  `tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货手机号',
  `province` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省',
  `city` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '市',
  `county` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区',
  `address_detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '详细地址',
  `post_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮政编码',
  `address_default` enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '是否默认 1默认 ',
  PRIMARY KEY (`address_id`) USING BTREE,
  INDEX `fk_address_uid`(`u_id`) USING BTREE,
  CONSTRAINT `fk_address_uid` FOREIGN KEY (`u_id`) REFERENCES `account` (`u_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `a_id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `p_id` int(4) NOT NULL COMMENT '权限id',
  `username` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `create_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `login_time` datetime NULL DEFAULT NULL COMMENT '登录时间',
  PRIMARY KEY (`a_id`) USING BTREE,
  INDEX `fk_p_id`(`p_id`) USING BTREE,
  CONSTRAINT `fk_p_id` FOREIGN KEY (`p_id`) REFERENCES `power` (`p_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 1, '阿政123', 'admin', '1369501150@qq.com', '4c8a6f740bba2faa8dcf7be927bca1cb', '2021-11-18 10:53:32', '2021-11-18 10:53:32');
INSERT INTO `admin` VALUES (2, 2, '孔乙己', 'asdfsa', '1169734099', 'b4aad043e90ffba2902669f913e93474', '2021-08-11 19:38:36', NULL);

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `cart_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '购物车id',
  `u_id` int(4) NOT NULL COMMENT '用户id',
  `g_id` int(4) NOT NULL COMMENT '商品id',
  `spec_id` int(4) NOT NULL COMMENT '商品规格id',
  `num` int(4) NOT NULL COMMENT '数量',
  `c_add_time` datetime NOT NULL COMMENT '添加时间',
  `c_update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`cart_id`) USING BTREE,
  INDEX `fk_uid_cart`(`u_id`) USING BTREE,
  INDEX `fk_gid_cart`(`g_id`) USING BTREE,
  INDEX `fk_gsid_cart`(`spec_id`) USING BTREE,
  CONSTRAINT `fk_gid_cart` FOREIGN KEY (`g_id`) REFERENCES `goods` (`g_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_gsid_cart` FOREIGN KEY (`spec_id`) REFERENCES `goods_specs` (`spec_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_uid_cart` FOREIGN KEY (`u_id`) REFERENCES `account` (`u_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `category_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '商品类目id',
  `category_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类目名称',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, '服饰');
INSERT INTO `category` VALUES (2, '鞋子');
INSERT INTO `category` VALUES (3, '皮具箱包');
INSERT INTO `category` VALUES (4, '珠宝饰品');
INSERT INTO `category` VALUES (5, '手表眼镜');
INSERT INTO `category` VALUES (6, '妆品');
INSERT INTO `category` VALUES (7, '户外运动');
INSERT INTO `category` VALUES (8, '家具/建材');
INSERT INTO `category` VALUES (9, '食品/保健');
INSERT INTO `category` VALUES (10, '母婴/儿童用品');
INSERT INTO `category` VALUES (11, '家电');
INSERT INTO `category` VALUES (12, '数码/电脑/手机');
INSERT INTO `category` VALUES (13, '汽车/配件');
INSERT INTO `category` VALUES (14, '图书');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `comment_id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `g_id` int(4) NOT NULL COMMENT '商品id',
  `spec_id` int(4) NOT NULL COMMENT '商品规格id',
  `b_id` int(4) NOT NULL COMMENT '商家id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评价内容',
  `img` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '评价图片',
  `goods_rate` tinyint(1) UNSIGNED NOT NULL COMMENT '商品评分',
  `store_rate` tinyint(1) UNSIGNED NOT NULL COMMENT '商家评分',
  `express_rate` tinyint(1) UNSIGNED NOT NULL COMMENT '物流评分',
  `comment_add_time` datetime NOT NULL COMMENT '评价时间',
  PRIMARY KEY (`comment_id`) USING BTREE,
  INDEX `fk_comment_g_id`(`g_id`) USING BTREE,
  INDEX `fk_comment_b_id`(`b_id`) USING BTREE,
  INDEX `fk_comment_spec_id`(`spec_id`) USING BTREE,
  CONSTRAINT `fk_comment_b_id` FOREIGN KEY (`b_id`) REFERENCES `store` (`b_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_comment_g_id` FOREIGN KEY (`g_id`) REFERENCES `goods` (`g_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_comment_spec_id` FOREIGN KEY (`spec_id`) REFERENCES `goods_specs` (`spec_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `g_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `g_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
  `b_id` int(4) NOT NULL COMMENT '商家id',
  `g_src` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品图片',
  `category_id` int(4) NOT NULL COMMENT '商品类目',
  `g_status` enum('审核','正常','下架') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '审核' COMMENT '商品状态',
  `g_create_time` datetime NOT NULL COMMENT '上架时间',
  PRIMARY KEY (`g_id`) USING BTREE,
  INDEX `fk_category_id`(`category_id`) USING BTREE,
  INDEX `fk_b_id`(`b_id`) USING BTREE,
  CONSTRAINT `fk_b_id` FOREIGN KEY (`b_id`) REFERENCES `store` (`b_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES (5, '欧莱雅注白瓶淡斑痘印美白377精华烟酰胺水杨酸补水', 4, 'goods/e9de423fe5ced3265c129ed05b5e0183.jpg,goods/6a2613919d440e57a04c752b937070d7.jpg', 6, '正常', '2021-08-19 10:12:26');
INSERT INTO `goods` VALUES (7, '神舟战神Z7/Z7T 11代酷睿i5/i7 RTX3050独显15.6吋144Hz学生便携办公电竞游戏笔记本电脑', 3, 'goods/575e935c092761e1cda94a70455c1815.jpg,goods/fe0ff914f18ad54512eeaf8489194368.jpg,goods/f4086cf60f4b120b1bf27a2eee1c0afd.jpg,goods/84357f2d97f48ac8600199862023daa0.jpg', 12, '正常', '2021-08-19 17:44:23');
INSERT INTO `goods` VALUES (8, 'Hasee/神舟 战神 Z7 i5/i7吃鸡游戏本GTX独显学生笔记本电脑办公', 3, 'goods/6f716c4e0854f5edaf083df4f1f64999.jpg,goods/7ccd7f8683e8c204b9620a0ed88cb260.jpg,goods/c27e11bb044687c83f2005d16c7d44bb.jpg', 12, '正常', '2021-09-17 16:32:39');
INSERT INTO `goods` VALUES (9, '棉短袖t恤男2021夏季薄款冰感纯色白色半袖体恤潮', 1, 'goods/809c587228fcba7baabdf093df1524a6.jpg,goods/9b8e54189c1b9ba57958702489785dc9.jpg,goods/73ef45b62a8fadd57d35b5b9142457bb.jpg', 1, '正常', '2021-09-17 17:07:30');
INSERT INTO `goods` VALUES (10, 'PREDATOR FREAK .3 L TF 男子硬人造草坪足球运动鞋', 5, 'goods/9cb0994e51ad3d4c2c37729f23b0e98e.jpg,goods/213c47d26c797b36133d632df95e30a2.jpg,goods/4031a5a9897438c5f82394dc9e3d3efd.jpg,goods/920927416717d552962afab586edfd20.jpg,goods/cdf8ce9d5adc8ae2fe0b0aa73f5380b7.jpg', 7, '正常', '2021-09-24 10:41:13');
INSERT INTO `goods` VALUES (11, 'abibas男装运动健身加厚连帽夹克外套GN6834', 5, 'goods/8192fce4cb994414ffef52819638ffe2.jpg,goods/d2f05ccb3d94689346e264a9e5d87d43.jpg,goods/22abb78ea4f2bc0eb4b8a1a2a7bc2673.jpg,goods/98f1b84d57f2d8abcba39af880a06860.jpg', 1, '正常', '2021-09-24 10:51:39');
INSERT INTO `goods` VALUES (12, 'abibasneo QUESTAR FLOW男子休闲运动鞋F36241', 5, 'goods/a55df7ece8ebc4bdd3ad688589fe75b0.jpg,goods/5b78843a137ca7f82afcd2e5b42005b2.jpg,goods/9a352de6c8c97d4c56d5a267d92de618.jpg,goods/3bfbb3f75bd97b8dcd71aba354e15ba9.jpg', 2, '正常', '2021-09-24 10:56:33');
INSERT INTO `goods` VALUES (13, 'abibasss TH PNT WV FUNCT 男装训练运动裤装GP0953 黑色/黑色', 5, 'goods/6854bb54a14dcf702704361cfea7c4d0.jpg,goods/b58c945546140b48b9aa12e16a7120c8.jpg,goods/a0593607c8830d9ccb1e821e946d9fe7.jpg,goods/e4bac9810cbf86f42ac38659718dd101.jpg', 1, '正常', '2021-09-24 14:06:47');
INSERT INTO `goods` VALUES (14, 'abibas WINDBREAKER BOS 女装春秋运动型格修身夹克外套FT2862 白/淡灰 A/L(170/92A)', 5, 'goods/229a52dedfd57b22ee6fbb605238a8ea.jpg,goods/5569593311d4a12cfefa4faa5d6fe197.jpg,goods/ea3f928d4b09ee3ee0d614c316de1602.jpg,goods/77284ce08b1f96acb2fa2293acc49450.jpg', 1, '正常', '2021-09-24 14:16:26');
INSERT INTO `goods` VALUES (15, 'abibas三叶草 VDay WB 女装春秋运动外套H39027 白/浅猩红', 5, 'goods/3bf18e99321b6ee79ff5ba59e374ecc2.jpg,goods/a4accf58d5153b50629d13833f9b3386.jpg,goods/583a797d28934b698e22414c4194658b.jpg,goods/c3ac801e745b00fe3f0f39cfd8b16e49.jpg', 1, '正常', '2021-09-24 14:20:30');
INSERT INTO `goods` VALUES (16, '第七代小棕瓶特润修护精华50ml面部精华液滋润紧致补水抗老淡细纹护肤品化妆品礼盒', 4, 'goods/97c1703a6ab3a0e5ed9506a3f36e2e79.jpg,goods/d6bb7383a313ee444306398ecb64560d.jpg,goods/acd025c85320650a10dfbca1533c1a11.jpg,goods/58c0c3707833674d7fa89a78acc27f6d.jpg', 6, '正常', '2021-09-24 14:30:25');
INSERT INTO `goods` VALUES (17, '控油除螨男士洗面奶 抗痘洁面乳150ml(去油去黑头角质痘印 去螨虫）洗脸', 4, 'goods/03ac1e9ef3796d5df8afab81bc066531.jpg,goods/c969bfb71bcd50dddde2e3f0ae6a1797.jpg,goods/d398c0f717771b3c2752b02f215c0329.jpg,goods/fa5a94a5d979b9ce80aaa3b1271401fd.jpg', 6, '正常', '2021-11-18 11:10:56');
INSERT INTO `goods` VALUES (18, '山茶花氨基酸洗面奶100g*2 （洁面乳男女洗面奶 敏感肌护肤品深层清洁补水保湿）', 4, 'goods/ec9f67352038dc031c0836038c2006e3.jpg,goods/7c8adf1b0ff9f7daf3df2ae9e083c128.jpg,goods/48fc0a7034b4a80f8475ae31b7fc7068.jpg,goods/84c42731b4ffee989878043a68832d27.jpg', 6, '正常', '2021-11-18 11:18:37');
INSERT INTO `goods` VALUES (19, '男士洗面奶男美白除螨套装学生控油洁面乳男生去角质洗脸泡沫去黑头【持证除螨】美白洗面奶', 4, 'goods/a37fbaa2989058b6926ec761f0b06832.png,goods/8d0fa5517fc063cac3ac170f1d7b57a9.jpg,goods/2e931001e2284d6dd281b14a935b32bf.jpg', 6, '正常', '2021-11-18 11:25:41');

-- ----------------------------
-- Table structure for goods_specs
-- ----------------------------
DROP TABLE IF EXISTS `goods_specs`;
CREATE TABLE `goods_specs`  (
  `spec_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '规格id',
  `g_id` int(4) NOT NULL COMMENT '商品id',
  `spec_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品规格名称',
  `sell_price` decimal(10, 2) NOT NULL COMMENT '售卖价格',
  `discounted_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '折扣价',
  `stock` int(4) NOT NULL DEFAULT 0 COMMENT '库存',
  `buy_num` int(4) NOT NULL DEFAULT 0 COMMENT '销量',
  `spec_status` enum('正常','下架') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '正常' COMMENT '规格状态',
  PRIMARY KEY (`spec_id`) USING BTREE,
  INDEX `fk_g_id`(`g_id`) USING BTREE,
  CONSTRAINT `fk_g_id` FOREIGN KEY (`g_id`) REFERENCES `goods` (`g_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 72 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of goods_specs
-- ----------------------------
INSERT INTO `goods_specs` VALUES (1, 7, '战神Z7-TA5NA：i5-11260H/RTX3050/8G/512G/15.6英寸IPS屏', 5899.00, NULL, 0, 11, '正常');
INSERT INTO `goods_specs` VALUES (2, 7, '战神Z7-TA5NS：i5-11400H/RTX3050/16G/512G/15.6英寸144Hz屏', 6299.00, NULL, 4, 3, '正常');
INSERT INTO `goods_specs` VALUES (3, 7, '战神Z7T-TA5NS：i5-11400H/RTX3050Ti/16G/512G/15.6英寸IPS屏', 6399.00, NULL, 0, 2, '正常');
INSERT INTO `goods_specs` VALUES (4, 7, '战神Z7-TA7NP：i7-11800H/RTX3050/16G/512G/15.6英寸144Hz屏', 6899.00, 6699.00, 0, 2, '正常');
INSERT INTO `goods_specs` VALUES (5, 5, '全套只需199', 199.00, NULL, 98, 2, '正常');
INSERT INTO `goods_specs` VALUES (6, 5, '单瓶补水', 99.00, 95.99, 95, 4, '正常');
INSERT INTO `goods_specs` VALUES (7, 8, '官方标配8GB+512GB', 4999.00, 4899.00, 8, 2, '正常');
INSERT INTO `goods_specs` VALUES (8, 8, '官方标配16GB+512GB', 5399.00, 5299.00, 19, 0, '正常');
INSERT INTO `goods_specs` VALUES (9, 9, 'S码', 65.99, NULL, 100, 0, '下架');
INSERT INTO `goods_specs` VALUES (10, 9, 'M码', 65.99, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (11, 9, 'L码', 65.99, NULL, 100, 0, '下架');
INSERT INTO `goods_specs` VALUES (12, 9, 'XL码', 75.99, NULL, 20, 0, '下架');
INSERT INTO `goods_specs` VALUES (13, 10, '39码', 599.00, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (14, 10, '40码', 599.00, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (15, 10, '41码', 599.00, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (16, 10, '42码', 599.00, NULL, 86, 2, '正常');
INSERT INTO `goods_specs` VALUES (17, 10, '43码', 599.00, NULL, 21, 0, '正常');
INSERT INTO `goods_specs` VALUES (18, 10, '44码', 599.00, NULL, 12, 0, '正常');
INSERT INTO `goods_specs` VALUES (19, 10, '45码', 599.00, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (20, 10, '46码', 599.00, NULL, 70, 0, '正常');
INSERT INTO `goods_specs` VALUES (21, 11, '170/88A', 636.00, NULL, 20, 0, '正常');
INSERT INTO `goods_specs` VALUES (22, 11, '175/92A', 636.00, NULL, 50, 0, '正常');
INSERT INTO `goods_specs` VALUES (23, 11, '175/95A', 636.00, NULL, 32, 1, '正常');
INSERT INTO `goods_specs` VALUES (24, 11, '180/100A', 636.00, NULL, 88, 0, '正常');
INSERT INTO `goods_specs` VALUES (25, 11, '185/104A', 636.00, NULL, 123, 0, '正常');
INSERT INTO `goods_specs` VALUES (26, 12, '39码', 386.00, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (27, 12, '40码', 386.00, NULL, 499, 1, '正常');
INSERT INTO `goods_specs` VALUES (28, 12, '41码', 386.00, NULL, 500, 0, '正常');
INSERT INTO `goods_specs` VALUES (29, 12, '42码', 386.00, NULL, 98, 0, '正常');
INSERT INTO `goods_specs` VALUES (30, 12, '43码', 386.00, NULL, 96, 0, '正常');
INSERT INTO `goods_specs` VALUES (31, 12, '44码', 386.00, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (32, 12, '45码', 386.00, NULL, 77, 0, '正常');
INSERT INTO `goods_specs` VALUES (33, 13, 'A/XS(170/72A)', 386.00, NULL, 50, 0, '正常');
INSERT INTO `goods_specs` VALUES (34, 13, 'A/S(175/76A)', 386.00, NULL, 22, 0, '正常');
INSERT INTO `goods_specs` VALUES (35, 13, 'A/M(175/80A)', 386.00, NULL, 2983, 0, '正常');
INSERT INTO `goods_specs` VALUES (36, 13, 'A/L(180/86A)', 386.00, NULL, 3542, 2, '正常');
INSERT INTO `goods_specs` VALUES (37, 13, 'A/XL(185/90A)', 386.00, NULL, 245, 0, '正常');
INSERT INTO `goods_specs` VALUES (38, 13, 'A/2XL(185/96A)', 386.00, NULL, 1004, 0, '正常');
INSERT INTO `goods_specs` VALUES (39, 13, 'A/3XL(190/100A)', 386.00, NULL, 215, 0, '正常');
INSERT INTO `goods_specs` VALUES (40, 14, 'A/XS(155/80A)', 358.00, NULL, 321, 0, '正常');
INSERT INTO `goods_specs` VALUES (41, 14, 'A/S(160/84A)', 358.00, NULL, 64, 0, '正常');
INSERT INTO `goods_specs` VALUES (42, 14, 'A/M(165/88A)', 358.00, NULL, 165, 0, '正常');
INSERT INTO `goods_specs` VALUES (43, 14, 'A/L(170/92A)', 358.00, NULL, 353, 0, '正常');
INSERT INTO `goods_specs` VALUES (44, 14, 'A/XL(170/96A', 358.00, NULL, 156, 0, '正常');
INSERT INTO `goods_specs` VALUES (45, 14, 'A/2XL(175/100A)', 358.00, NULL, 154, 0, '正常');
INSERT INTO `goods_specs` VALUES (46, 15, '30(参考身高:160~165CM)', 484.00, NULL, 135, 0, '正常');
INSERT INTO `goods_specs` VALUES (47, 15, '32(参考身高:160~165CM)', 484.00, NULL, 1223, 0, '正常');
INSERT INTO `goods_specs` VALUES (48, 15, '34(参考身高:164~167CM)', 484.00, NULL, 215, 0, '正常');
INSERT INTO `goods_specs` VALUES (49, 15, '36(参考身高:166~170CM)', 484.00, NULL, 215, 0, '正常');
INSERT INTO `goods_specs` VALUES (50, 15, '38(参考身高:169~172CM)', 484.00, NULL, 2123, 0, '正常');
INSERT INTO `goods_specs` VALUES (51, 15, '40(参考身高:170~175CM)', 484.00, NULL, 67, 0, '正常');
INSERT INTO `goods_specs` VALUES (52, 16, '小棕瓶精华50ml', 900.00, NULL, 213, 0, '正常');
INSERT INTO `goods_specs` VALUES (53, 16, '蓝光眼霜15ml', 680.00, NULL, 211, 0, '正常');
INSERT INTO `goods_specs` VALUES (54, 16, '眼绷带精华15ml', 1800.00, NULL, 15, 0, '正常');
INSERT INTO `goods_specs` VALUES (55, 16, '精华+蓝光眼霜', 5000.00, NULL, 10, 0, '正常');
INSERT INTO `goods_specs` VALUES (56, 16, '精华+眼霜+樱花水', 22000.00, NULL, 12, 0, '正常');
INSERT INTO `goods_specs` VALUES (57, 16, '打斑弹精华50ml', 280.00, NULL, 150, 0, '正常');
INSERT INTO `goods_specs` VALUES (58, 17, '控油除螨抗痘洁面乳150ml', 29.90, NULL, 98, 2, '正常');
INSERT INTO `goods_specs` VALUES (59, 17, '冰爽活炭洁面乳150ml', 36.90, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (60, 17, '控油冰爽洁面泡沫150ml', 45.90, NULL, 98, 2, '正常');
INSERT INTO `goods_specs` VALUES (61, 18, '洗面奶（100g）', 79.90, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (62, 18, '洗面奶（100g*2）', 158.00, NULL, 500, 0, '正常');
INSERT INTO `goods_specs` VALUES (63, 18, '面膜（5片装）', 89.50, NULL, 1000, 0, '正常');
INSERT INTO `goods_specs` VALUES (64, 18, '洁面水乳3件套', 318.00, NULL, 1000, 0, '正常');
INSERT INTO `goods_specs` VALUES (65, 18, '水乳2件套', 248.00, NULL, 1000, 0, '正常');
INSERT INTO `goods_specs` VALUES (66, 18, '水乳精华3件套', 319.00, NULL, 1000, 0, '正常');
INSERT INTO `goods_specs` VALUES (67, 19, '【持证除螨】美白洗面奶', 79.90, NULL, 999, 0, '正常');
INSERT INTO `goods_specs` VALUES (68, 19, '【细致毛孔】洗面奶150ml+黑头导出液+精华液', 354.50, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (69, 19, '【美白爽肤】洗面奶+精华爽肤水', 153.70, NULL, 998, 0, '正常');
INSERT INTO `goods_specs` VALUES (70, 19, '【美白洁面】洗面奶+竹炭除螨皂', 88.80, NULL, 100, 0, '正常');
INSERT INTO `goods_specs` VALUES (71, 19, '【美白嫩肤】洗面奶+去角质', 99.99, NULL, 1000, 0, '正常');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `m_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `title` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `path` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单路径',
  `keepAlive` enum('false','true','') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'false' COMMENT '保持活性',
  `order` int(4) NOT NULL DEFAULT 1 COMMENT '菜单排序',
  `parent_m_id` int(4) NULL DEFAULT NULL COMMENT '父级菜单id',
  PRIMARY KEY (`m_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, '商品', 'icon_shangpin', '/goods', 'true', 1, 0);
INSERT INTO `menu` VALUES (2, '商品列表', 'icon_list', '/list', 'false', 1, 1);
INSERT INTO `menu` VALUES (3, '商品添加', 'icon_checkmore1', '/add', 'false', 2, 1);
INSERT INTO `menu` VALUES (4, '设置', 'icon_set', '/set', 'false', 1, 0);
INSERT INTO `menu` VALUES (5, '菜单设置', 'icon_menu', '/menu', 'false', 1, 4);
INSERT INTO `menu` VALUES (6, '权限设置', 'icon_relationship', '/power', 'false', 1, 4);
INSERT INTO `menu` VALUES (7, '用户设置', 'icon_infopersonal', '/user', 'false', 2, 4);
INSERT INTO `menu` VALUES (8, '商品类目', 'icon_bookresource', '/category', 'false', 5, 1);
INSERT INTO `menu` VALUES (9, '店铺', 'icon_custom', '/store', 'true', 3, 0);
INSERT INTO `menu` VALUES (10, '店铺列表', 'icon_list', '/list', 'true', 1, 9);
INSERT INTO `menu` VALUES (11, '添加店铺', 'icon_edit', '/add', 'true', 2, 9);

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `order_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `u_id` int(4) NOT NULL COMMENT '用户id',
  `order_num` int(4) NOT NULL COMMENT '订单数量',
  `order_price` decimal(10, 2) NOT NULL COMMENT '订单价格',
  `tel` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货手机号',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货地址',
  `express_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单号',
  `order_status` enum('已支付','已发货','已收货','已评价','待退款','已退款') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '已支付' COMMENT '订单状态',
  `order_create_time` datetime NOT NULL COMMENT '订单创建时间',
  `order_delivery_time` datetime NULL DEFAULT NULL COMMENT '订单发货时间',
  `order_receipt_time` datetime NULL DEFAULT NULL COMMENT '订单收货时间',
  `order_finish_time` datetime NULL DEFAULT NULL COMMENT '订单完成时间',
  PRIMARY KEY (`order_id`) USING BTREE,
  INDEX `fk_order_u_id`(`u_id`) USING BTREE,
  CONSTRAINT `fk_order_u_id` FOREIGN KEY (`u_id`) REFERENCES `account` (`u_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for order_list
-- ----------------------------
DROP TABLE IF EXISTS `order_list`;
CREATE TABLE `order_list`  (
  `order_detail_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '订单详情id',
  `order_id` int(4) NOT NULL COMMENT '订单id',
  `b_id` int(4) NOT NULL COMMENT '商家id',
  `g_id` int(4) NOT NULL COMMENT '商品id',
  `spec_id` int(4) NOT NULL COMMENT '规格id',
  `price` decimal(10, 2) NOT NULL COMMENT '价格',
  `num` int(4) NOT NULL COMMENT '数量',
  PRIMARY KEY (`order_detail_id`) USING BTREE,
  INDEX `fk_order_list_o_id`(`order_id`) USING BTREE,
  INDEX `fk_order_list_g_id`(`g_id`) USING BTREE,
  INDEX `fk_order_list_spec_id`(`spec_id`) USING BTREE,
  INDEX `fk_oder_list_b_id`(`b_id`) USING BTREE,
  CONSTRAINT `fk_oder_list_b_id` FOREIGN KEY (`b_id`) REFERENCES `store` (`b_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_order_list_g_id` FOREIGN KEY (`g_id`) REFERENCES `goods` (`g_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_order_list_o_id` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_order_list_spec_id` FOREIGN KEY (`spec_id`) REFERENCES `goods_specs` (`spec_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for power
-- ----------------------------
DROP TABLE IF EXISTS `power`;
CREATE TABLE `power`  (
  `p_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `p_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `m_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联菜单id',
  PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of power
-- ----------------------------
INSERT INTO `power` VALUES (1, '管理员', '1,2,3,4,5,6,7,8,9,10,11,12');
INSERT INTO `power` VALUES (2, '测试', '1,2');

-- ----------------------------
-- Table structure for store
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store`  (
  `b_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '店铺id',
  `b_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '店铺名称',
  `b_src` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '店铺头像',
  `b_account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '店铺账号',
  `b_email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '店铺邮箱',
  `b_password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录密码',
  `b_desc` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '店铺描述',
  `b_status` enum('审核','正常','冻结','封号') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '审核' COMMENT '店铺状态',
  `b_create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`b_id`) USING BTREE,
  UNIQUE INDEX `u_email`(`b_email`) USING BTREE COMMENT '唯一邮箱',
  UNIQUE INDEX `u_name`(`b_name`) USING BTREE COMMENT '唯一店铺名',
  UNIQUE INDEX `u_account`(`b_account`) USING BTREE COMMENT '唯一账号'
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of store
-- ----------------------------
INSERT INTO `store` VALUES (1, '美羊羊的服装店', 'store/5f35599fdf6e7d5a35389037ae5c77ec.jpg', 'meiyangyang168', '1369501150@qq.com', 'zzz12345', '好看的衣服，这里都有！', '正常', '2021-08-12 10:49:15');
INSERT INTO `store` VALUES (2, '酷酷的鞋', 'store/e7b111bdcb0936592110ca4472b9fbf9.jpg', 'kukudexie168', '15648125@qq.com', 'qq123456', '这里的鞋子啥都有', '正常', '2021-08-13 06:38:01');
INSERT INTO `store` VALUES (3, '不卡的电脑铺', 'store/c8b1a2fca6d0b64c25d3ffcf3402e43c.jpg', 'bukadediannaopu', '4568772@qq.com', 'qq123456', '我卖的电脑，从来不卡！', '正常', '2021-08-15 11:47:55');
INSERT INTO `store` VALUES (4, '神奇的彩妆店', 'store/e239c2b9170f248b48c93c0b1e9a1114.jpg', 'caizhuangdian', 'sdasdasd@qq.com', 'qq123456', '在我这里没有好看，只有更好看！', '正常', '2021-08-18 11:38:02');
INSERT INTO `store` VALUES (5, 'abibas', 'store/378511b430590cf758a800631022369b.jpg', 'abibas', 'abibas@qq.com', 'abibas123456', '国内知名品牌', '正常', '2021-09-24 10:37:57');

-- ----------------------------
-- Table structure for user_like
-- ----------------------------
DROP TABLE IF EXISTS `user_like`;
CREATE TABLE `user_like`  (
  `like_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '收藏id',
  `u_id` int(4) NULL DEFAULT NULL COMMENT '用户id',
  `g_id` int(4) NULL DEFAULT NULL COMMENT '商品id',
  `l_add_time` datetime NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`like_id`) USING BTREE,
  INDEX `fk_like_u_id`(`u_id`) USING BTREE,
  INDEX `fk_like_g_id`(`g_id`) USING BTREE,
  CONSTRAINT `fk_like_g_id` FOREIGN KEY (`g_id`) REFERENCES `goods` (`g_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_like_u_id` FOREIGN KEY (`u_id`) REFERENCES `account` (`u_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
