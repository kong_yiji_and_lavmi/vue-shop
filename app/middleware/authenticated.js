export default function({ store, redirect, route }) {
  // If the user is not authenticated
  // 客户端判断
  if (!process.client) return;
  const isLoginPath = route.fullPath === "/login";
  if (!store.state.user && !isLoginPath) {
    return redirect("/login?path=" + route.fullPath);
  }
  if (isLoginPath && store.state.user) {
    redirect("/");
  }
}
