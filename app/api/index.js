import qs from "qs";
export default ({ $axios }, inject) => {
  const { $post } = $axios;
  function $get(url, data) {
    return $axios.$get(url + qs.stringify(data, { addQueryPrefix: true }));
  }
  const APIS = [
    {
      // 首页轮播
      url: "getlunbo",
      methods: "get",
      name: "GetSwiper"
    },
    {
      // 发送邮箱验证码
      url: "sendcode",
      methods: "post",
      name: "SendCode"
    },
    {
      // 注册
      url: "register",
      methods: "post",
      name: "Register"
    },
    {
      // 登录
      url: "login",
      methods: "post",
      name: "Login"
    },
    {
      // 获取商品列表
      url: "getgoodslist",
      methods: "get",
      name: "GetGoodsList"
    },
    {
      // 获取商品信息
      url: "getgoodsinfo",
      methods: "get",
      name: "GetGoodsInfo"
    },
    {
      // 商品页面添加购物车
      url: "useraddtocart",
      methods: "post",
      name: "AddCart"
    },
    {
      // 获取用户购物车
      url: "usercartlist",
      methods: "get",
      name: "GetCart"
    },
    {
      // 获取商品规格
      url: "getgoodsspecs",
      methods: "get",
      name: "GetSpecs"
    },
    {
      // 更新购物车规格
      url: "userupdatecartspecs",
      methods: "post",
      name: "UpdateCartSpecs"
    },
    {
      // 更新购物车数量
      url: "userupdatecartnum",
      methods: "post",
      name: "UpdateCartNum"
    },
    {
      // 购物车移除到收藏
      url: "usercarttolike",
      methods: "post",
      name: "CartToLike"
    },
    {
      // 删除购物车
      url: "userdeletecart",
      methods: "post",
      name: "DelCart"
    },
    {
      // 获取商品种类
      url: "getgoodscategory",
      methods: "get",
      name: "GetGoodsCategory"
    },
    {
      // 获取用户地址信息
      url: "usergetaddress",
      methods: "get",
      name: "GetAddress"
    },
    {
      // 用户添加地址
      url: "useraddaddress",
      methods: "post",
      name: "AddAddress"
    },
    {
      // 用户地址详情
      url: "usergetaddressdetail",
      methods: "get",
      name: "AddressDetail"
    },
    {
      // 用户更新地址
      url: "userupdateaddress",
      methods: "post",
      name: "UpdateAddress"
    },
    {
      // 用户从购物车提交订单
      url: "useraddorder",
      methods: "post",
      name: "AddOrder"
    },
    {
      // 用户订单列表
      url: "usergetorder",
      methods: "get",
      name: "GetOrder"
    },
    {
      // 更新订单状态
      url: "userupdateorder",
      methods: "post",
      name: "UpdateOrder"
    },
    {
      // 从商品页提交订单
      url: "usergoodstoorder",
      methods: "post",
      name: "GoodsToOrder"
    },
    {
      // 获取用户收藏列表
      url: "usergetuserlike",
      methods: "get",
      name: "GetUserLike"
    },
    {
      // 添加收藏
      url: "useraddlike",
      methods: "post",
      name: "AddLike"
    },
    {
      // 删除收藏
      url: "userdellike",
      methods: "post",
      name: "DelLike"
    },
    {
      // 收藏页删除收藏
      url: "userdellikes",
      methods: "post",
      name: "DelChooseLike"
    },
    {
      // 获取订单信息
      url: "usergetorderinfo",
      methods: "get",
      name: "GetOrderInfo"
    },
    {
      // 提交评论
      url: "usersubmitcomment",
      methods: "post",
      name: "SubmitComment"
    },
    {
      // 我的评论
      url: "usergetcommentlist",
      methods: "get",
      name: "GetCommentList"
    },
    {
      //删除地址
      url: "userdeladdress",
      methods: "post",
      name: "DelAddress"
    }
  ];

  // 报错接口
  inject("handleError", data => $post("/local/handleError", data));

  APIS.forEach(({ url, methods, name }) => {
    if (methods.toLocaleLowerCase() === "get") {
      inject(`api${name}`, data => $get(`/api/${url}`, data));
    } else {
      inject(`api${name}`, data => $post(`/api/${url}`, data));
    }
  });
};
