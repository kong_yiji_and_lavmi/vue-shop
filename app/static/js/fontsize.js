(function GoMobile() {
  function resizeRoot() {
    var Dpr = 1;
    var uAgent = window.navigator.userAgent;
    var isIOS = uAgent.match(/iphone/i);
    var isYIXIN = uAgent.match(/yixin/i);
    var is2345 = uAgent.match(/Mb2345/i);
    var ishaosou = uAgent.match(/mso_app/i);
    var isSogou = uAgent.match(/sogoumobilebrowser/gi);
    var isLiebao = uAgent.match(/liebaofast/i);
    var isGnbr = uAgent.match(/GNBR/i);
    var wWidth =
        screen.width > 0
          ? window.innerWidth >= screen.width || window.innerWidth == 0
            ? screen.width
            : window.innerWidth
          : window.innerWidth,
      wDpr,
      wFsize;
    var wHeight =
      screen.height > 0
        ? window.innerHeight >= screen.height || window.innerHeight == 0
          ? screen.height
          : window.innerHeight
        : window.innerHeight;
    if (window.devicePixelRatio) {
      wDpr = window.devicePixelRatio;
    } else {
      wDpr = isIOS ? (wWidth > 818 ? 3 : wWidth > 480 ? 2 : 1) : 1;
    }
    if (isIOS) {
      wWidth = screen.width;
      wHeight = screen.height;
    }
    if (wWidth > wHeight) {
      wWidth = wHeight;
    }
    wFsize = wWidth > 1080 ? 144 : wWidth / 7.5;
    wFsize = wFsize > 32 ? wFsize : 32;
    window.screenWidth_ = wWidth;
    if (isYIXIN || is2345 || ishaosou || isSogou || isLiebao || isGnbr) {
      // YIXIN 和 2345 这里有个刚调用系统浏览器时候的bug，需要一点延迟来获取
      setTimeout(function() {
        wWidth =
          screen.width > 0
            ? window.innerWidth >= screen.width || window.innerWidth == 0
              ? screen.width
              : window.innerWidth
            : window.innerWidth;
        wHeight =
          screen.height > 0
            ? window.innerHeight >= screen.height || window.innerHeight == 0
              ? screen.height
              : window.innerHeight
            : window.innerHeight;
        wFsize = wWidth > 1080 ? 144 : wWidth / 7.5;
        wFsize = wFsize > 32 ? wFsize : 32;
        document.getElementsByTagName("html")[0].style.fontSize = wFsize + "px";
      }, 500);
    } else {
      // document.getElementsByTagName('html')[0].dataset.dpr = wDpr;
      document.getElementsByTagName("html")[0].style.fontSize = wFsize + "px";
    }
    // alert("fz="+wFsize+";dpr="+window.devicePixelRatio+";UA="+uAgent+";width="+wWidth+";sw="+screen.width+";wiw="+window.innerWidth+";wsw="+window.screen.width+window.screen.availWidth);
  }

  function isPC() {
    var UA = navigator.userAgent.toLowerCase();
    return !(
      UA.indexOf("phone") != -1 ||
      UA.indexOf("mobile") != -1 ||
      UA.indexOf("android") != -1 ||
      UA.indexOf("ipad") != -1 ||
      UA.indexOf("ipod") != -1
    );
  }
  function alertTip() {
    alert(
      "请使用移动端设备打开此网站\nPlease Using mobile devices to open the WebSite!"
    );
  }
  if (!isPC()) {
    resizeRoot();
  } else {
    var timer;
    timer = setInterval(() => {
      if (isPC()) {
        alertTip();
      } else {
        clearInterval(timer);
        resizeRoot();
      }
    }, 3000);
  }
})();
