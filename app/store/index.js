export const state = () => ({
  user: null,
  token: null
});

export const mutations = {
  setUser(state, data) {
    state.user = data;
  },
  setToken(state, data) {
    state.token = data;
  }
};

export const getters = {
  storeUser: state => state.user
};
