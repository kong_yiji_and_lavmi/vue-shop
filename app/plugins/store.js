import { COOKIE_USER, COOKIE_TOKEN } from "../common";
export default ({ store, ...app }) => {
  if (!store.state.user) {
    const user = app.$getStorageData(COOKIE_USER, true);
    user && store.commit("setUser", user);
  }
  if (!store.state.token) {
    const token = app.$getStorageData(COOKIE_TOKEN, true);
    token && store.commit("setToken", token);
  }
};
