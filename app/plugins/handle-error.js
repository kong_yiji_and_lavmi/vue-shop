import Vue from "vue";
function handlerError(error, vm, hook) {
  console.log(error);
  vm &&
    vm.$handleError({
      message: error.message,
      name: error.name,
      stack: error.stack,
      hook
    });
  return true;
}
Vue.config.errorHandler = handlerError;

Vue.prototype.$throw = handlerError;

Vue.filter("formatSrc", function(src) {
  if (!src) {
    return "";
  }
  return process.env.IMG + src.split(",")[0];
});
Vue.filter("getBuyNum", function(arr) {
  if (!Array.isArray(arr)) {
    return 0;
  }
  return arr.reduce((a, c) => a + c.buy_num, 0);
});

Vue.filter("goodsUrl", function(id) {
  if (!id) {
    return "/";
  }
  return "/goods/" + id;
});

Vue.filter("storeUrl", function(id) {
  if (!id) {
    return "/";
  }
  return "/store/" + id;
});
const floatNumRegxep = /\./g;
Vue.filter("cartPrice", function(num, sellPrice, discountPrice) {
  if (discountPrice) {
    let price = num * sellPrice;
    return floatNumRegxep.test(price) ? price.toFixed(2) : price;
  }
  let price = num * sellPrice;
  return floatNumRegxep.test(price) ? price.toFixed(2) : price;
});
