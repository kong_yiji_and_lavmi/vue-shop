import { COOKIE_TOKEN, COOKIE_USER } from "../common";
// 错误信息
const codeMessage = {
  200: "服务器成功返回请求的数据。",
  201: "新建或修改数据成功。",
  202: "一个请求已经进入后台排队（异步任务）。",
  204: "删除数据成功。",
  400: "发出的请求有错误，服务器没有进行新建或修改数据的操作。",
  401: "用户没有权限（令牌、用户名、密码错误）。",
  403: "用户得到授权，但是访问是被禁止的。",
  404: "发出的请求针对的是不存在的记录，服务器没有进行操作。",
  406: "请求的格式不可得。",
  410: "请求的资源被永久删除，且不会再得到的。",
  422: "当创建一个对象时，发生一个验证错误。",
  500: "服务器发生错误，请检查服务器。",
  502: "网关错误。",
  503: "服务不可用，服务器暂时过载或维护。",
  504: "网关超时。"
};

export default function({ $axios, app, redirect, store, req, route }) {
  // 请求报错处理
  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status);
    const isLogin = !(code === 401 || code === 403);
    if (process.server) {
      // 服务端
      function hasDir(dir) {
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        return dir;
      }
      const fs = require("fs");
      const dayjs = require("dayjs");
      const path = require("path");
      const LOGDIR = path.resolve(process.cwd(), "./log");
      const todayFile = dayjs().format("YYYY-MM-DD") + ".log";
      const now = dayjs().format("YYYY-MM-DD HH:mm:ss");
      const filePath = path.join(LOGDIR, todayFile);
      const { message, stack } = error;
      const isHasFile = fs.existsSync(filePath);
      hasDir(LOGDIR);
      const str = `错误信息：${message}
错误位置：${stack}  
时间：${now}\n\n`;
      const dealFn = isHasFile ? fs.appendFileSync : fs.writeFileSync;
      dealFn(filePath, str, "utf8");
    } else {
      // 客户端
      app.$Toast({
        type: "fail",
        message: codeMessage[code] || "请求失败\n请检查网络状态！"
      });
      if (!isLogin) {
        // 清除客户端
        app.$rmCookie(COOKIE_TOKEN, COOKIE_USER);
        app.$rmStorageData(true, COOKIE_TOKEN, COOKIE_USER);
        store.commit("setUser", null);
        store.commit("setToken", null);
      }
    }
    if (!isLogin) {
      redirect("/login?path=" + route.fullPath);
    }
  });
  // 响应拦截
  $axios.onResponse(response => {
    if (
      response &&
      response.data &&
      response.data.status !== 0 &&
      process.client
    ) {
      app.$Toast({
        type: "fail",
        message: response.data.msg
      });
    }
    return response;
  });
  // 请求拦截
  $axios.onRequest(config => {
    config.timeout = 15 * 1000;
    // 默认使用客户端 token
    let token = store.state.token;
    if (process.server) {
      // 如果是服务端 就使用 浏览器cookie
      const cookie = app.$formatCookie(req.headers.cookie);
      try {
        token = JSON.parse((cookie && cookie[COOKIE_TOKEN]) || "null");
      } catch (error) {}
    }
    if (token) {
      config.headers["authorization"] = token;
    }
    return config;
  });
}
